#!/bin/bash
#
# Run the gwdetchar.lasso module on a daily stride

. ~/.bash_profile
if [[ "$(whoami)" == "detchar" ]]; then
    unset X509_USER_PROXY
fi
set -e

# set bash environment
here_=$(cd "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))" && pwd)
. ${here_}/functions
echo "-- Loaded local functions"

# set python environment
activate_conda_environment
echo "-- Environment set"
echo "Conda prefix: ${CONDA_PREFIX}"

# get run date
gpsstart=`gps_start_yesterday`
gpsend=`gps_start_today`
date_=`yyyymmdd ${gpsstart}`
# duration=$((gpsend-gpsstart))
echo "-- Identified run date as ${date_}"

# set up output directory
htmlbase=${HOME}/public_html/lasso
outdir=${htmlbase}/full-results/day/${date_}
summarydir=${htmlbase}/summary/day/${date_}

# get IFO
if [[ "`hostname -f`" == *"ligo-la"* ]]; then
    export IFO="L1"
elif [[ "`hostname -f`" == *"ligo-wa"* ]]; then
    export IFO="H1"
else
    echo "Cannot determine IFO" 1>&2 && false
fi
set +e

nagios_status 0 "Daily lasso analysis for ${date_} is running" 10000 "Daily lasso analysis for ${date_} is taking too long" > ${htmlbase}/nagios.json

LOGFILE="/tmp/daily-lasso-$(whoami).err"
cmd="python -m gwdetchar.lasso -i $IFO -p ${IFO}:DMT-SNSW_EFFECTIVE_RANGE_MPC.mean --remove-bad-chans --remove-outliers=2.5 -o $outdir -s $summarydir $gpsstart $gpsend"
echo "$ $cmd" && eval $cmd 2> ${LOGFILE}

EXITCODE="$?"

# write JSON output
TIMER=100000
TIMEOUT="Daily lasso analysis has not run since ${date_}"
if [ "$EXITCODE" -eq 0 ]; then
    MESSAGE="Daily lasso analysis for ${date_} complete"
    nexit=0
else
    MESSAGE=`echo -n "Daily lasso analysis for ${date_} failed with exitcode $EXITCODE\\\n" && sed ':a;N;$!ba;s/\n/\\\n/g' ${LOGFILE} | tr '"' "'"`
    nexit=2
fi
nagios_status $nexit "$MESSAGE" $TIMER "$TIMEOUT" > ${htmlbase}/nagios.json
exit ${EXITCODE}
